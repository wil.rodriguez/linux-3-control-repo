class profile::base {
  include ntp

  $packages = ['bash-completion', 'tmux', 'screen', 'tree']

  package { $packages:
    ensure => installed,
  }
}
