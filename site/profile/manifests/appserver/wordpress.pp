# Class: profile::appserver::wordpress
#
#
class profile::appserver::wordpress {
  include wordpress
  include apache
  include mysql::server
  include apache::mod::php

  package { ['php-mysqlnd', 'php-mysql']:
    ensure => 'installed',
    notify => Exec['title'],
  }

  exec { 'title':
    command     => "echo ${facts[hostname]}",
    provider    => 'shell',
    refreshonly => true,
  }

  file { '/etc/test.conf':
    ensure  => file,
    content => epp('profile/appserver/wordpress/test.epp', {
      'keys_enable'     => true,
      'keys_file'       => '/etc/sshd',
      'keys_trusted'    => ['1', '2'],
      'keys_requestkey' => 'blah',
      'keys_controlkey' => 'blah2',
    })
  }
}
